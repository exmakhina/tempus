from setuptools import setup, find_packages

setup(
	name="xm_tempus",
	packages=find_packages(include=["exmakhina.tempus"]),
	package_dir = {'exmakhina.tempus': '.'},
	py_modules=[
	 "exmakhina.tempus.__init__",
	 "exmakhina.tempus.tempus",
	],
	install_requires=[
	 "lmdb",
	],
)
