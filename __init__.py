#!/usr/bin/env python
# -*- coding: utf-8 vi:noet
# SPDX-FileCopyrightText: 2019-2020,2022-2023 Jérôme Carretero <cJ@zougloub.eu> & contributors
# SPDX-License-Identifier: MIT
# 2019-03-07 - Time and UUID Representation

import io
import struct
import time
import datetime
import logging
import bisect
import typing as t

logger = logging.getLogger(__name__)


class AmbiguousUnixTimeError(ValueError):
	pass


def load_leaps(path="/usr/share/zoneinfo/leap-seconds.list") \
 -> t.Tuple[t.Sequence[t.Tuple[int,int]],int]:
	"""
	Parse the leap seconds file

	:return: UNIX times for leap transitions and the corresponding new leaps,
	 TAI time and leaps, and file validity limit

	"""

	unix_to_leaps = []
	tai_to_leaps = []
	t_1970 = 2208988800
	with io.open(path, "r") as fi:
		for line in fi:
			line, comment = line.split("#", 1)
			line = line.rstrip()
			if comment.startswith("@"):
				until = int(comment.split()[1]) - t_1970
			if not line:
				continue
			t_1900, leap = line.split()
			t_1900 = int(t_1900)
			t_unix = t_1900 - t_1970
			leap = int(leap)
			unix_to_leaps.append((t_unix, leap))
			t_tai_a = t_unix + leap-1
			t_tai_b = t_unix + leap+0
			tai_to_leaps.append((t_tai_a, leap-1, leap))
			tai_to_leaps.append((t_tai_b, leap, leap))
	return unix_to_leaps, tai_to_leaps, until


class TimeProvider:
	"""
	TimeProvider provides the time in TAI or (UNIX, leaps);
	if the system has a TAI clock, the time can always be provided,
	otherwise the system can't represent time during leap second
	events.
	"""
	def __init__(self):
		self.assess()

	def assess(self) -> None:
		self._unix_to_leaps, self._tai_to_leaps, self.leaps_until = load_leaps()

		try:
			time.CLOCK_TAI
		except AttributeError:
			logger.warning("TAI not available in Python")
			while True:
				t_unix = time.clock_gettime(time.CLOCK_REALTIME)
				i_unix = int(t_unix)
				try:
					leaps_from_file = self.get_leaps_from_file(i_unix)
				except AmbiguousUnixTimeError:
					# No luck
					continue

				break
			self._has_tai = False
			self._good_tai = False
			self._leaps = leaps_from_file
			return

		while True:
			t_tai = time.clock_gettime(time.CLOCK_TAI)
			t_unix = time.clock_gettime(time.CLOCK_REALTIME)
			i_unix = int(t_unix)
			i_tai = int(t_tai)
			f_tai = t_tai - i_tai
			f_unix = t_unix - i_unix

			if f_tai > f_unix:
				continue

			try:
				leaps_from_file = self.get_leaps_from_file(i_unix)
			except AmbiguousUnixTimeError:
				# No luck
				continue

			break

		leaps = i_tai - i_unix

		if leaps == 0:
			logger.warning("TAI not available")
			self._has_tai = False
			self._good_tai = False
		else:
			self._has_tai = True

			if leaps_from_file == leaps:
				logger.info("TAI is good (for now)")
				self._good_tai = True
			else:
				logger.warning("TAI not good, expected %d leaps but got %d",
				 leaps_from_file, leaps)
				self._good_tai = False

		self._leaps = leaps_from_file

	def get_leaps_from_file(self, i_unix, fold=None) -> int:
		"""
		Get leap seconds corresponding to a UNIX timestamp
		"""
		if fold == 1:
			i_unix += 1

		idx_leaps = bisect.bisect_right(self._unix_to_leaps, (i_unix, 123456))
		idx_leaps -= 1
		leaps_i_unix, leaps_from_file = self._unix_to_leaps[idx_leaps]
		if leaps_i_unix == i_unix:
			if fold is None:
				raise AmbiguousUnixTimeError(i_unix)
		return leaps_from_file

	def get_leaps_from_file_tai(self, i_tai) -> t.Tuple[int,int]:
		"""
		Get leap seconds corresponding to a TAI timestamp
		"""
		idx_leaps = bisect.bisect_right(self._tai_to_leaps, (i_tai, 123456))
		idx_leaps -= 1
		leaps_i_tai, leaps, next_leaps = self._tai_to_leaps[idx_leaps]
		return leaps, next_leaps

	def tai(self) -> float:
		"""
		Get current TAI time

		Raises AmbiguousUnixTimeError if queried during a leap second event,
		and we don't have a proper TAI clock.
		"""
		if self._has_tai and self._good_tai:
			t_tai = time.clock_gettime(time.CLOCK_TAI)
			t_unix = t_tai - self._leaps
			if t_unix > self.leaps_until:
				self.assess()
			return t_tai
		else:
			t_unix = time.clock_gettime(time.CLOCK_REALTIME)
			leaps = self.get_leaps_from_file(t_unix)
			t_tai = t_unix + leaps
			if t_unix > self.leaps_until:
				self.assess()
			return t_tai

	def unix(self) -> t.Tuple[float,int]:
		"""
		Get current UNIX time, and number of leap seconds

		Raises AmbiguousUnixTimeError if queried during a leap second event,
		and we don't have a proper TAI clock.
		"""

		if self._has_tai and self._good_tai:
			t_tai = time.clock_gettime(time.CLOCK_TAI)
			leaps, next_leaps = self.get_leaps_from_file_tai(t_tai)
			t_unix = t_tai - leaps
			if t_unix > self.leaps_until:
				self.assess()
			return t_unix, leaps

		else:
			t_unix = time.clock_gettime(time.CLOCK_REALTIME)
			if t_unix > self.leaps_until:
				self.assess()
			i_unix = int(t_unix)
			leaps = self.get_leaps_from_file(i_unix)

		return t_unix, self._leaps

	def tai_from_datetime(self, dt):
		t_unix = dt.timestamp()
		i_unix = int(t_unix)
		leaps = self.get_leaps_from_file(i_unix, fold=dt.fold)
		t_tai = t_unix + leaps
		return t_tai

	def datetime_from_tai(self, t_tai):
		i_tai = int(t_tai)
		leaps, next_leaps = self.get_leaps_from_file_tai(i_tai)
		if leaps != next_leaps:
			fold = 1
			leaps = next_leaps
		else:
			fold = 0
		t_unix = t_tai - leaps
		dt = datetime.datetime.utcfromtimestamp(t_unix).replace(tzinfo=datetime.timezone.utc, fold=fold)
		return dt


tp = None


def now():
	"""
	"""

	global tp

	if tp is None:
		tp = TimeProvider()

	return tp.tai()


class Tempus(object):
	"""
	Precisely represent time, using a TAI clock
	"""

	__annotations__ = dict(_s=int, _ns=int)
	__slots__ = tuple(__annotations__.keys())

	#def __new__(cls, *args, **kwargs):
	#	logger.info("Creating %s", cls)
	#	return super(Tempus, cls).__new__(cls, *args, **kwargs)

	def __init__(self, s, ns):
		"""
		"""
		self._s = s
		self._ns = ns

	@staticmethod
	def from_datetime(dt):
		global tp
		if tp is None:
			tp = TimeProvider()

		if dt is None:
			# Unreliable
			dt = datetime.datetime.utcnow().replace(tzinfo=datetime.timezone.utc)

		t_tai = tp.tai_from_datetime(dt)
		s = int(t_tai)
		ns = int(round((t_tai - s) * 1e9))

		return Tempus(s, ns)

	def to_datetime(self):
		global tp
		if tp is None:
			tp = TimeProvider()
		i_tai = self._s
		t_tai = i_tai + self._ns * 1e-9

		return tp.datetime_from_tai(t_tai)

	def __eq__(self, other):
		return self._s == other._s and self._ns == other._ns

	def __int__(self):
		return int(round(float(self)))

	def __float__(self):
		return self._s + self._ns * 1e-9

	def __gt__(self, other):
		return (self._s, self._ns) > (other._s, other._ns)

	def __lt__(self, other):
		return (self._s, self._ns) < (other._s, other._ns)

	def __ge__(self, other):
		return (self._s, self._ns) >= (other._s, other._ns)

	def __le__(self, other):
		return (self._s, self._ns) <= (other._s, other._ns)

	def __sub__(self, other):
		s = self._s - other._s
		ns = self._ns - other._ns
		while ns < 0:
			s -= 1
			ns += 1000000000
		while ns > 1000000000:
			s += 1
			ns -= 1000000000
		return s + ns*1e-9

	def __add__(self, other):
		s = self._s + other._s
		ns = self._ns + other._ns

		while ns < 0:
			s -= 1
			ns += 1000000000
		while ns > 1000000000:
			s += 1
			ns -= 1000000000

		ret = type(self)(s=s, ns=ns)
		return ret


def tempus_dumps(t):
	"""
	"""

	s = t._s
	ns = t._ns

	data = struct.pack(">qI", t._s, t._ns)

	return data


def tempus_loads(data):
	s, ns = struct.unpack(">qI", data)
	return Tempus(s, ns)


__all__ = (
 "TimeProvider",
 "AmbiguousUnixTimeError",
 "now",
 "Tempus",
 "tempus_loads",
 "tempus_dumps",
)
