######
Tempus
######


This library is used to represent (terrestrial) time and time
differences, reliably, using a scalar.

This library uses TAI time as a scalar representation of time;
it provides a TimeProvider class which can obtain TAI time.

Time can also be unambiguously represented using Python
datetime.datetime (considering use of the fold flag is made, and it has an
implicit or explicit timezone), or with a UNIX time **and** a leap
seconds or a fold flag.

However, the Python standard library doesn't consider leap seconds
when computing time differences from datetime.datetime.

This library provides means to convert between TAI time, UNIX time,
and Python datetime.datetime.
